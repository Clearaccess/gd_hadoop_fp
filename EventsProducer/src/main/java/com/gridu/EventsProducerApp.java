package com.gridu;

import static com.gridu.util.impl.PropertiesLoaderImpl.PropertiesNames;

import com.gridu.beans.Event;
import com.gridu.util.EventsGenerator;
import com.gridu.util.FlumeClient;
import com.gridu.util.PropertiesLoader;
import com.gridu.util.impl.EventsGeneratorImpl;
import com.gridu.util.impl.FlumeClientImpl;
import com.gridu.util.impl.PropertiesLoaderImpl;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class EventsProducerApp {

    private static final String EVENTS_COUNT = "events.count";

    public static void main(String[] args) throws URISyntaxException, IOException {

        PropertiesLoader propertiesLoader = new PropertiesLoaderImpl();
        Properties settings = propertiesLoader.loadProperty(PropertiesNames.SETTINGS.toString());
        FlumeClient client = new FlumeClientImpl(settings);

        //Path out = Paths.get(Thread.currentThread().getContextClassLoader().getResource("events.csv").toURI());
        //client.start(Files.newOutputStream(out, StandardOpenOption.WRITE));
        client.start();
        EventsGenerator generator = new EventsGeneratorImpl();

        List<Event> events = new ArrayList<>();
        for (int i = 0; i < Integer.parseInt(settings.getProperty(EVENTS_COUNT)); i++) {

            events.add(generator.generateEvent());
        }

        client.sendEvents(events);
        System.out.println(String.format("Events sent [%s]", events.size()));
        client.stop();
    }
}
