package com.gridu.beans;

import com.opencsv.bean.CsvBindByPosition;

import java.time.LocalDate;

public class Event {

    @CsvBindByPosition(position = 0)
    private String productName;

    @CsvBindByPosition(position = 1)
    private String productCategory;

    @CsvBindByPosition(position = 2)
    private String clientIp;

    @CsvBindByPosition(position = 3)
    private LocalDate purchaseDate;

    @CsvBindByPosition(position = 4)
    private Double productPrice;

    public Event(String productName, String productCategory, String clientIp,
            LocalDate purchaseDate, Double productPrice) {
        this.productName = productName;
        this.productCategory = productCategory;
        this.clientIp = clientIp;
        this.purchaseDate = purchaseDate;
        this.productPrice = productPrice;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(String productCategory) {
        this.productCategory = productCategory;
    }

    public String getClientIp() {
        return clientIp;
    }

    public void setClientIp(String clientIp) {
        this.clientIp = clientIp;
    }

    public LocalDate getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(LocalDate purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public Double getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(Double productPrice) {
        this.productPrice = productPrice;
    }
}