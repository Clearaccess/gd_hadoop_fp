package com.gridu.util;

import com.gridu.beans.Event;

public interface EventsGenerator {

    Event generateEvent();
}
