package com.gridu.util;

import com.gridu.beans.Event;
import java.io.OutputStream;
import java.util.List;

public interface FlumeClient {

    void start();

    void start(OutputStream outputStream);

    void sendEvent(Event event);

    void sendEvents(List<Event> events);

    void stop();
}
