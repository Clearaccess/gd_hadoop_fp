package com.gridu.util;

import java.util.List;
import java.util.Properties;

public interface PropertiesLoader {

    Properties loadProperty(String propertiesName);

    List<String> loadListPropertiesValues(String propertiesName);
}
