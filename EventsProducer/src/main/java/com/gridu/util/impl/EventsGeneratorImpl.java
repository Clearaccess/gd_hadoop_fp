package com.gridu.util.impl;

import static com.gridu.util.impl.PropertiesLoaderImpl.PropertiesNames;

import com.gridu.beans.Event;
import com.gridu.util.EventsGenerator;
import com.gridu.util.PropertiesLoader;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class EventsGeneratorImpl implements EventsGenerator {

    private static final Random RND = new Random();
    private static final String INIT_DATE = "2018-01-01";
    private static final int MAX_WEEK_RANGE = 23;

    private List<String> product_names;
    private List<String> product_categories;

    public EventsGeneratorImpl() {

        PropertiesLoader propertiesLoader = new PropertiesLoaderImpl();
        this.product_names = propertiesLoader
                .loadListPropertiesValues(PropertiesNames.PRODUCT_NAMES.toString());
        this.product_categories = propertiesLoader
                .loadListPropertiesValues(PropertiesNames.PRODUCT_CATEGORIES.toString());
    }

    @Override
    public Event generateEvent() {

        String productName = generateProductName();
        String productCategory = generateProductCategory();
        String clientIp = generateClientIp();
        LocalDate purchaseDate = generatePurchaseDate();
        Double productPrice = generateProductPrice();

        return new Event(productName, productCategory, clientIp, purchaseDate, productPrice);
    }

    private String generateProductName() {

        return product_names.get(RND.nextInt(product_names.size()));
    }

    private String generateProductCategory() {

        return product_categories.get(RND.nextInt(product_categories.size()));
    }

    private String generateClientIp() {

        int[] parts = new int[4];
        for (int i = 0; i < 4; i++) {
            parts[i] = RND.nextInt(256);
        }

        return Arrays.stream(parts).mapToObj(String::valueOf).collect(Collectors.joining("."));
    }

    private LocalDate generatePurchaseDate() {
        return LocalDate.parse(INIT_DATE).plusWeeks(RND.nextInt(MAX_WEEK_RANGE));
    }

    private Double generateProductPrice() {
        return Math.abs(RND.nextGaussian()) * 1000.0 + Math.abs(RND.nextGaussian()) * 100.0;
    }
}
