package com.gridu.util.impl;

import com.gridu.util.PropertiesLoader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

public class PropertiesLoaderImpl implements PropertiesLoader {

    public static enum PropertiesNames {
        PRODUCT_CATEGORIES("product_categories"),
        PRODUCT_NAMES("product_names"),
        SETTINGS("settings");

        private String propertyName;

        PropertiesNames(String propertyName) {
            this.propertyName = propertyName;
        }

        @Override
        public String toString() {
            return this.propertyName;
        }
    }

    @Override
    public Properties loadProperty(String propertiesName) {

        InputStream propertyStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(propertiesName + ".properties");
        /*String propertyPath = null;
        try {
            propertyPath = URLDecoder.decode(propertyURL.getPath(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }*/

        Properties properties = new Properties();
        try {
            properties.load(propertyStream);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return properties;
    }

    @Override
    public List<String> loadListPropertiesValues(String propertiesName) {

        Properties properties = loadProperty(propertiesName);
        return properties.stringPropertyNames().stream().map(properties::getProperty)
                .collect(Collectors.toList());
    }
}
