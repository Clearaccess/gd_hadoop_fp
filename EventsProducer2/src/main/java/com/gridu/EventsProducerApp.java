package com.gridu;

import static com.gridu.util.impl.PropertiesLoaderImpl.PropertiesNames;

import com.gridu.util.EventsGenerator;
import com.gridu.util.FlumeClient;
import com.gridu.util.PropertiesLoader;
import com.gridu.util.impl.EventsGeneratorImpl;
import com.gridu.util.impl.FlumeClientImpl;
import com.gridu.util.impl.PropertiesLoaderImpl;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Properties;

public class EventsProducerApp {

    private static final String EVENTS_COUNT = "events.count";

    public static void main(String[] args) throws IOException {

        PropertiesLoader propertiesLoader = new PropertiesLoaderImpl();
        Properties settings = propertiesLoader.loadProperty(PropertiesNames.SETTINGS.toString());
        FlumeClient client = new FlumeClientImpl(settings);

        client.start();
        EventsGenerator generator = new EventsGeneratorImpl();

        for (int i = 0; i < Integer.parseInt(settings.getProperty(EVENTS_COUNT)); i++) {
            client.sendEvent(generator.generateEvent());
        }
        client.stop();
    }
}
