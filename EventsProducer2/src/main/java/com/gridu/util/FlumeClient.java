package com.gridu.util;

import com.gridu.beans.Event;
import java.io.OutputStream;

public interface FlumeClient {

    void start();

    void start(OutputStream outputStream);

    void sendEvent(Event event);

    void stop();
}
