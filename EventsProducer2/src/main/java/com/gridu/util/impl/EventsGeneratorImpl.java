package com.gridu.util.impl;

import static com.gridu.util.impl.PropertiesLoaderImpl.PropertiesNames;

import com.gridu.beans.Event;
import com.gridu.util.EventsGenerator;
import com.gridu.util.PropertiesLoader;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class EventsGeneratorImpl implements EventsGenerator {

    private static final Random RND = new Random();
    private static final String INIT_DATE = "2018-01-01";
    private static final int MAX_WEEK_RANGE = 23;

    private List<String> product_names;
    private List<String> product_categories;

    public EventsGeneratorImpl() {

        PropertiesLoader propertiesLoader = new PropertiesLoaderImpl();
        this.product_names = propertiesLoader
                .loadListPropertiesValues(PropertiesNames.PRODUCT_NAMES.toString());
        this.product_categories = propertiesLoader
                .loadListPropertiesValues(PropertiesNames.PRODUCT_CATEGORIES.toString());
    }

    @Override
    public Event generateEvent() {

        String productName = generateProductName();
        String productCategory = generateProductCategory();
        String clientIp = generateClientIp();
        Date purchaseDate = generatePurchaseDate();
        Double productPrice = generateProductPrice();

        return new Event(productName, productCategory, clientIp, purchaseDate, productPrice);
    }

    private String generateProductName() {

        return product_names.get(RND.nextInt(product_names.size()));
    }

    private String generateProductCategory() {

        return product_categories.get(RND.nextInt(product_categories.size()));
    }

    private String generateClientIp() {

        int[] parts = new int[4];
        for (int i = 0; i < 4; i++) {
            parts[i] = RND.nextInt(256);
        }

        StringBuilder temp = new StringBuilder();

        for (int i = 0; i < parts.length; i++) {
            temp.append(parts[i]);
            if (i != parts.length - 1) {
                temp.append(".");
            }
        }

        return temp.toString();
    }

    private Date generatePurchaseDate() {

        Calendar calendar = Calendar.getInstance();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            calendar.setTime(dateFormat.parse(INIT_DATE));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        calendar.add(Calendar.WEEK_OF_YEAR, RND.nextInt(MAX_WEEK_RANGE));
        Date date = calendar.getTime();
        return date;
    }

    private Double generateProductPrice() {
        return Math.abs(RND.nextGaussian()) * 1000.0 + Math.abs(RND.nextGaussian()) * 100.0;
    }
}
