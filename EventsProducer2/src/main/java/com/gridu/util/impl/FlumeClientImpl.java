package com.gridu.util.impl;

import com.gridu.beans.Event;
import com.gridu.util.FlumeClient;
import com.opencsv.CSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.Socket;
import java.util.Properties;

public class FlumeClientImpl implements FlumeClient {

    private static final String HOST_PROPERTY = "server.host";
    private static final String PORT_PROPERTY = "server.port";

    private Properties properties;
    private Socket clientSocket;
    private Writer writer;
    private StatefulBeanToCsv<Event> csvWriter;


    public FlumeClientImpl(Properties properties) {

        this.properties = properties;
    }

    @Override
    public void start() {

        try {
            this.clientSocket = new Socket(properties.getProperty(HOST_PROPERTY),
                    Integer.parseInt(properties.getProperty(PORT_PROPERTY)));
            start(clientSocket.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void start(OutputStream outputStream) {

        this.writer = new OutputStreamWriter(outputStream);
        this.csvWriter = new StatefulBeanToCsvBuilder<Event>(writer)
                .withSeparator(CSVWriter.DEFAULT_SEPARATOR)
                .build();
    }

    @Override
    public void stop() {

        try {
            this.writer.close();
            if(clientSocket!=null) {
                this.clientSocket.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void sendEvent(Event event) {
        try {
            csvWriter.write(event);
        } catch (CsvRequiredFieldEmptyException e) {
            e.printStackTrace();
        } catch (CsvDataTypeMismatchException e) {
            e.printStackTrace();
        }
    }
}
