--hive -hiveconf hive.root.logger=INFO,console

-- 4. Create external Hive table to process data
-- Create DB
CREATE DATABASE IF NOT EXISTS avaniukov;

USE avaniukov;

-- Set parameters for work with multi level directory
SET mapred.input.dir.recursive = true;
SET hive.mapred.supports.subdirectories = true;

-- Create External Table
CREATE EXTERNAL TABLE IF NOT EXISTS purchases
  (product_name STRING,
   product_category STRING,
   client_ip STRING,
   purchase_date STRING,
   product_price DOUBLE
   )
   ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
   STORED AS TEXTFILE
   LOCATION 'hdfs:////user/avaniukov/hadoop_final_project/flume/events';

-- Set parameters for dynamic partition
SET hive.exec.dynamic.partition = true;
SET hive.exec.dynamic.partition.mode = nonstrict;

-- Create Partition Table
CREATE TABLE part_purchases
  (product_name STRING,
   product_category STRING,
   client_ip STRING,
   product_price DOUBLE
   )
   PARTITIONED BY (purchase_date STRING)
   STORED AS TEXTFILE;

-- Load data into table
   FROM purchases
   INSERT OVERWRITE TABLE part_purchases PARTITION(purchase_date)
          SELECT product_name, product_category, client_ip, product_price, purchase_date;

-- 5. Execute complex select queries
-- 5.1. Select top 10  most frequently purchased categories
SELECT t1.product_category, regexp_replace (t1.amounts, '\"', '')
FROM (SELECT product_category, COUNT(*) as amounts
FROM purchases
GROUP BY product_category
SORT BY amounts DESC
LIMIT 10) t1;
-- Upload result
INSERT OVERWRITE DIRECTORY 'hdfs:////user/avaniukov/hadoop_final_project/hive/TOP_10_PURCHASED_CATEGORIES'
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
STORED AS TEXTFILE
SELECT NULL, product_category, COUNT(*) as amounts
FROM purchases
GROUP BY product_category
SORT BY amounts DESC
LIMIT 10;

-- 5.2. Select top 10 most frequently purchased product in each category
select t1.product_name, t1.product_category, t1.amount from
    (select product_name, product_category, count(*) amount, row_number() over (partition by product_category order by count(*) desc) num
    from purchases
    group by product_name, product_category) t1
where t1.num<=10;
-- Upload result
INSERT OVERWRITE DIRECTORY 'hdfs:////user/avaniukov/hadoop_final_project/hive/TOP_10_PURCHASED_PRODUCTS_EACH_CATEGORIES'
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
STORED AS TEXTFILE
select NULL, t1.product_name, t1.product_category, t1.amount
from
    (select product_name, product_category, count(*) amount, row_number() over (partition by product_category order by count(*) desc) num
    from purchases
    group by product_name, product_category) t1
where t1.num<=10;

-- 6. JOIN events with geodata
-- 6.1.
add jar <path>/<jar_name>;
create function extract_country as 'com.gridu.GeoIP';
-- 6.2. JOIN events data with ip geo data
select product_name, product_category, client_ip, product_price, extract_country(client_ip)
from purchases;
-- Upload result
INSERT OVERWRITE DIRECTORY 'hdfs:////user/avaniukov/hadoop_final_project/hive/PURCHASES_WITH_IP_COUNTRY'
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
STORED AS TEXTFILE
select product_name, product_category, client_ip, product_price, extract_country(client_ip) country
from purchases;

-- 6.3. Select top 10 countries with the highest money spending
select sum(t.product_price) spend, t.country
from (select extract_country(client_ip) country, product_price from purchases) t
where t.country IS NOT NULL
group by t.country
order by spend desc
limit 10;
-- Upload result
INSERT OVERWRITE DIRECTORY 'hdfs:////user/avaniukov/hadoop_final_project/hive/TOP_10_COUNTRY_BY_SPEND'
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
STORED AS TEXTFILE
select NULL, sum(t.product_price) spend, t.country
from (select extract_country(client_ip) country, product_price from purchases) t
where t.country IS NOT NULL
group by t.country
order by spend desc
limit 10;