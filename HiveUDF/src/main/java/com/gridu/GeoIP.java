package com.gridu;

import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.maxmind.geoip2.model.CountryResponse;
import com.maxmind.geoip2.record.Country;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import org.apache.hadoop.hive.ql.exec.UDF;

public final class GeoIP extends UDF {

    protected static final String DB_FILE_NAME = "GeoLite2-Country.mmdb";
    protected static DatabaseReader reader;

    static {
        try {
            InputStream database = Thread.currentThread().getContextClassLoader()
                .getResourceAsStream(DB_FILE_NAME);

            if (database == null) {
                throw new IllegalArgumentException("DB not found");
            }

            reader = new DatabaseReader.Builder(database).build();

        } catch (IOException | IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    //TODO Speed, static
    public String evaluate(final String ip) {

        String countryName = null;

        try {

            InetAddress ipAddress = InetAddress.getByName(ip);

            CountryResponse response = reader.country(ipAddress);

            Country country = response.getCountry();
            countryName = country.getName();
        } catch (IOException | GeoIp2Exception e) {
            //Something handler
        }

        return countryName;
    }
}
