import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

public class SimulationFlumeServer {

    private ServerSocket serverSocket;
    private Socket clientSocket;
    private BufferedReader in;

    public void start(int port) throws IOException, InterruptedException {

        serverSocket = new ServerSocket(port);

        System.out.println("Start server");
        System.out.println("Wait clients...");
        clientSocket = serverSocket.accept();
        in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

        String temp = null;
        while((temp = in.readLine())!=null){

            System.out.println(temp);
            while(!in.ready()){
                System.out.println("Wait data...");
                Thread.sleep(1000);
            }
        }
    }

    public void stop() throws IOException {

        in.close();
        clientSocket.close();
        serverSocket.close();
    }

    public static void main(String[] args) throws IOException, InterruptedException {

        SimulationFlumeServer server = new SimulationFlumeServer();
        server.start(7180);
        server.stop();
    }

}
