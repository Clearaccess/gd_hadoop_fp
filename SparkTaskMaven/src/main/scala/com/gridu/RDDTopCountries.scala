package com.gridu

import java.io.InputStream
import java.net.InetAddress

import com.maxmind.geoip2.DatabaseReader
import com.maxmind.geoip2.exception.AddressNotFoundException
import org.apache.spark.{SparkConf, SparkContext}

object RDDTopCountries {

  def main(args: Array[String]) {

    val sc = new SparkContext(new SparkConf()
      .setMaster("yarn-cluster")
      .setAppName("TopCountry"))

    val eventsPath = "hdfs:////user/avaniukov/hadoop_final_project/flume/events/2018/12/04/FlumeData.1543924519096.csv"

    val rddOrders = sc.textFile(eventsPath)
      .map(line => line.split(",").map(clean))

    val ipData = rddOrders.map(i => (i(2), i(4)))

    val data = ipData.mapPartitions(part => {

      val dbStream: InputStream = Thread.currentThread().getContextClassLoader.getResourceAsStream("GeoLite2-Country.mmdb")
      val reader = new DatabaseReader.Builder(dbStream).build()

      part.map(p => (p._2, {
        val ipAddress = InetAddress.getByName(p._1.toString)
        try {
          val response = reader.country(ipAddress)
          response.getCountry.getName
        }
        catch {
          case e: AddressNotFoundException => ""
        }
      }))
    }).map(_.swap).filter(i => i._1 != "")

    //Top 10 Countries

    data.mapValues(_.toString.toDouble)
      .reduceByKey(_ + _)
      .sortBy(i => i._2, ascending = false)
      .take(10)
      .foreach(println)
  }

  def clean(field: String): String = {

    val reg = """\"|\\"""
    field.replaceAll(reg,"").trim
  }
}
