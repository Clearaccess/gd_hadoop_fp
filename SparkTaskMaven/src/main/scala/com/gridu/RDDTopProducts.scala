package com.gridu

import org.apache.spark.mllib.rdd.MLPairRDDFunctions
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object RDDTopProducts {

  def main(args: Array[String]) {

    val sc = new SparkContext(new SparkConf()
      .setMaster("yarn-cluster")
      .setAppName("TopProducts"))

    val eventsPath = "hdfs:////user/avaniukov/hadoop_final_project/flume/events/2018/12/04/FlumeData.1543924519096.csv"git

    var rddEvents = sc.textFile(eventsPath)
      .map(line => line.split(",").map(clean))

    // Top 10 the most frequently purchased product in each category

    var tempRDD: RDD[(String, (Double, String))] = rddEvents
      .map(i => ((i(1), i(0)), i(4).toString.toDouble))
      .reduceByKey(_ + _)
      .map(i => (i._1._1, (i._2, i._1._2)))

    MLPairRDDFunctions.fromPairRDD(tempRDD)
      .topByKey(10)
      .foreach(show)
  }

  def clean(field: String): String = {

    val reg = """\"|\\"""
    field.replaceAll(reg, "").trim
  }

  def show(pair: (String, Array[(Double, String)])): Unit = {
    for (t <- pair._2) {
      println(pair._1, t._2, t._1)
    }
  }
}
