package com.gridu

import java.util.Properties

import org.apache.spark.sql.{Row, SQLContext}
import org.apache.spark.sql.functions.desc
import org.apache.spark.sql.types.{DoubleType, StringType, StructField, StructType}
import org.apache.spark.{SparkConf, SparkContext}

object DFTopCategories {

    def main(args: Array[String]){

        val sc = new SparkContext(new SparkConf()
          .setMaster("yarn-cluster")
          .setAppName("TopCategories"))

        val sqlc = new SQLContext(sc)

        // MySQL configs
        val prop = new Properties()
        prop.put("user", "root")
        prop.put("password", "cloudera")
        val url = "jdbc:mysql://ip-10-0-0-21.us-west-1.compute.internal:3306/avaniukov"

        val eventsPath = "hdfs:////user/avaniukov/hadoop_final_project/flume/events/2018/12/04/FlumeData.1543924519096.csv"

        val events = sc.textFile(eventsPath).map(line => line.split(",")
          .map(elem => elem.trim))
          .map(row => Row(row(0),row(1),row(2),row(3),row(4)))

        val schema = new StructType(Array(
            StructField("productName", StringType, true),
            StructField("productCategory", StringType, true),
            StructField("clientIp", StringType, true),
            StructField("purchaseDate", StringType, true),
            StructField("productPrice", DoubleType, true)))

        val df = sqlc.createDataFrame(events, schema)

        // Top 10 the most frequently appeared categories with DF
        var categoriesDF = df.groupBy("productCategory")
          .count()
          .sort(desc("count"))
          .limit(10)

        val tableColumnsNames = Seq("category","amount")

        categoriesDF = categoriesDF.toDF(tableColumnsNames: _*)

        categoriesDF.show()

        categoriesDF.write.mode("append").jdbc(url, "TOP_10_PURCHASED_CATEGORIES", prop)
    }
}
