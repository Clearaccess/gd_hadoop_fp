package com.gridu

import java.net.InetAddress
import java.util.Properties

import com.maxmind.geoip2.DatabaseReader
import com.maxmind.geoip2.exception.AddressNotFoundException
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.spark.sql.SQLContext
import org.apache.spark.sql.functions.desc
import org.apache.spark.{SparkConf, SparkContext}

object DFTopCountries {

  def main(args: Array[String]) {

    val sc = new SparkContext(new SparkConf()
      .setMaster("yarn-cluster")
      .setAppName("TopCountries"))
    val sql = new SQLContext(sc)

    val prop = new Properties()
    prop.put("user", "root")
    prop.put("password", "cloudera")
    val url = "jdbc:mysql://ip-10-0-0-21.us-west-1.compute.internal:3306/avaniukov"

    val eventsPath = "hdfs:////user/avaniukov/hadoop_final_project/flume/events/2018/12/04/FlumeData.1543924519096.csv"

    val rddEvents = sc.textFile(eventsPath).map(line => line.split(",").map(clean))

    val ipData = rddEvents.map(i => (i(2), i(4)))

    val data = ipData.mapPartitions(part => {

      val dbPath = "hdfs:////user/avaniukov/hadoop_final_project/util/GeoLite2-Country.mmdb"
      val fs = FileSystem.get(new Configuration())
      val in = fs.open(new Path(dbPath))
      val reader = new DatabaseReader.Builder(in).build()

      part.map(p => (p._2, {
        println("!!!")
        val ipAddress = InetAddress.getByName(p._1.toString)
        try {
          val response = reader.country(ipAddress)
          response.getCountry.getName
        }
        catch {
          case e: AddressNotFoundException => ""
        }
      }))
    }).map(_.swap).filter(i => i._1 != "")

    //Top 10 Countries

    val dfValidIps = sql.createDataFrame(
      data.filter(i => i._1 != null && i._2 != null)
        .map(i => (i._1.toString, i._2.toString))).toDF("country", "productPrice")

    var topDF = dfValidIps.groupBy("country")
      .agg(Map("productPrice" -> "sum"))
      .orderBy(desc("sum(productPrice)"))
      .withColumnRenamed("sum(productPrice)", "productPrice")
      .limit(10)

    val tableColumnsNames = Seq("country","spend")

    topDF = topDF.toDF(tableColumnsNames: _*)

    topDF.show()

    topDF.write.mode("append").jdbc(url, "TOP_10_COUNTRY_BY_SPEND", prop)
  }

  def clean(field: String): String = {

    val reg = """\"|\\"""
    field.replaceAll(reg,"").trim
  }
}
