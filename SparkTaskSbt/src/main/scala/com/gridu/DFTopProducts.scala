package com.gridu

import java.util.Properties

import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._
import org.apache.spark.sql.hive.HiveContext
import org.apache.spark.sql.types.{DoubleType, StringType, StructField, StructType}
import org.apache.spark.sql.{Row, SQLContext}
import org.apache.spark.{SparkConf, SparkContext}

object DFTopProducts {

  def main(args: Array[String]) {

    val sc = new SparkContext(new SparkConf()
      .setMaster("yarn-local")
      .setAppName("TopProducts"))

    var hiveContext = new HiveContext(sc)
    val sql = new SQLContext(sc)

    val prop = new Properties()
    prop.put("user", "root")
    prop.put("password", "cloudera")

    val url = "jdbc:mysql://ip-10-0-0-21.us-west-1.compute.internal:3306/avaniukov"

    val eventsPath = "hdfs:////user/avaniukov/hadoop_final_project/flume/events/2018/12/04/FlumeData.1543923925488.csv"

    val events = sc.textFile(eventsPath).map(line => line.split(",")
      .map(elem => elem.trim))
      .map(row => Row(row(0),row(1),row(2),row(3),row(4)))

    val schema = new StructType(Array(
      StructField("productName", StringType, true),
      StructField("productCategory", StringType, true),
      StructField("clientIp", StringType, true),
      StructField("purchaseDate", StringType, true),
      StructField("productPrice", DoubleType, true)))

    val df = hiveContext.createDataFrame(events, schema)

    // Top 10 the most frequently purchased product in each category

    val productsDF = df.select("productCategory","productName","productPrice")
    .groupBy("productCategory", "productName")
      .count()
      .withColumnRenamed("count", "amount")
      .withColumn("count", row_number().over(Window.partitionBy("productCategory").orderBy("productCategory")))
      .filter("count <= 10")
      .withColumnRenamed("productCategory", "category")
      .withColumnRenamed("productName", "product")
      .select("category", "product", "amount")
      .orderBy(desc("category"), desc("amount"))

    productsDF.show()

    productsDF.write.mode("append").jdbc(url, "TOP_10_PURCHASED_PRODUCTS_EACH_CATEGORIES", prop)
  }
}
