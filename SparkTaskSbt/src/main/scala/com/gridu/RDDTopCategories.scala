package com.gridu

import org.apache.spark.{SparkConf, SparkContext}

object RDDTopCategories {

  def main(args: Array[String]) {

    val sc = new SparkContext(new SparkConf()
      .setMaster("yarn-cluster")
      .setAppName("TopCategories"))

    val eventsPath = "hdfs:////user/avaniukov/hadoop_final_project/flume/events/2018/12/04/FlumeData.1543924519096.csv"

    val rddEvents = sc.textFile(eventsPath)
      .map(line => line.split(",").map(clean))

    // Top 10 the most frequently appeared categories with RDD
    rddEvents
      .map(i => i(1))
      .map(i => (i, 1))
      .reduceByKey(_ + _)
      .sortBy(_._2, ascending = false)
      .take(10)
      .foreach(println)
  }

  def clean(field: String): String = {

    val reg = """\"|\\"""
    field.replaceAll(reg,"").trim
  }
}
