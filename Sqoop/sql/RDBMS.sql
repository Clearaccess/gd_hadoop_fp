CREATE DATABASE avaniukov;

USE avaniukov;
-- 5.1
CREATE TABLE IF NOT EXISTS TOP_10_PURCHASED_CATEGORIES
(
    id bigint NOT NULL AUTO_INCREMENT,
    category varchar(100),
    amount varchar(100),
    CONSTRAINT TOP_10_PURCHASED_CATEGORIES_pk PRIMARY KEY (id)
);
-- 5.2
CREATE TABLE IF NOT EXISTS TOP_10_PURCHASED_PRODUCTS_EACH_CATEGORIES
(
    id bigint NOT NULL AUTO_INCREMENT,
    product varchar(100),
    category varchar(100),
    amount varchar(100),
    CONSTRAINT TOP_10_PURCHASED_PRODUCTS_EACH_CATEGORIES_pk PRIMARY KEY (id)
);
-- 6.2
CREATE TABLE IF NOT EXISTS PURCHASES_WITH_IP_COUNTRY
(
    id bigint NOT NULL AUTO_INCREMENT,
    product varchar(100),
    category varchar(100),
    client_ip varchar(100),
    price varchar(100),
    country varchar(100),
    CONSTRAINT PURCHASES_WITH_IP_COUNTRY_pk PRIMARY KEY (id)
);
-- 6.3
CREATE TABLE IF NOT EXISTS TOP_10_COUNTRY_BY_SPEND
(
    id bigint NOT NULL AUTO_INCREMENT,
    spend varchar(100),
    country varchar(100),
    CONSTRAINT TOP_10_COUNTRY_BY_SPEND_pk PRIMARY KEY (id)
);
