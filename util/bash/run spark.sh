#!/usr/bin/env bash
spark-submit \
--class com.gridu.RDDTopProducts \
 --master yarn \
 --conf "spark.driver.memory=256m" \
 --conf "spark.executor.memory=512m" \
 --deploy-mode cluster \
 --verbose \
 ./sparktasksbt_2.10-0.2.jar

spark-submit \
  --jars ./mysql-connector-java-5.1.12.jar,./geoip2-2.5.0.jar,./geoip2-2.6.0-sources.jar,./commons-codec-1.3.jar,./commons-logging-1.1.1.jar,./geoip2-2.6.0-javadoc.jar,./geoip2-2.6.0.jar,./google-http-client-1.21.0.jar,./httpclient-4.0.1.jar,./httpcore-4.0.1.jar,./jackson-annotations-2.6.0.jar,./jackson-core-2.6.4.jar,./jackson-databind-2.6.4.jar,./jsr305-1.3.9.jar,./maxmind-db-1.1.0.jar \
  --class com.gridu.RDDTopCountries \
  --master yarn \
  --conf "spark.driver.memory=256m" \
  --conf "spark.executor.memory=512m" \
  --deploy-mode cluster \
  ./sparktasksbt_2.10-0.2.jar

1.2.7